use std::collections::HashMap;

use std::{error, fmt};
use std::io::{Read, BufReader, BufRead};
use std::str::FromStr;

use crate::client::{Client, ClientParseError};
use crate::order::Order;

pub fn parse_clients(src: &mut dyn Read) -> Result<HashMap<String, Client>, ClientParseError> {
    let buffer = BufReader::new(src);

    let mut res: HashMap<String, Client> = HashMap::new();
    for line in buffer.lines() {
        let cl = Client::from_str(&line.unwrap())?;
        match res.insert(cl.get_name().to_string(), cl) {
            Some(_) => return Err(ClientParseError),
            None => (),
        }
    }
    Ok(res)
}

#[derive(Debug, Clone)]
pub struct ClientDoesntExistError;

impl fmt::Display for ClientDoesntExistError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Client has few stocks for saling")
    }
}

impl error::Error for ClientDoesntExistError {}

pub fn apply_orders(clients: &mut HashMap<String, Client>, orders: &mut dyn Read) -> Result<(), Box<dyn error::Error>> {
    let buffer = BufReader::new(orders);

    for line in buffer.lines() {
        let order = Order::from_str(&line.unwrap())?;
        match clients.get_mut(&order.client_name) {
            Some(client) => client.apply(
                order.is_buying, order.stock_type, order.price, order.count)?,
            None => return Err(Box::new(ClientDoesntExistError)),
        };
    }
    Ok(())
}
