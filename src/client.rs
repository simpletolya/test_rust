use std::str::FromStr;
use std::{fmt, error};
use std::collections::HashMap;

use crate::stock_type::{StockType};

#[derive(Debug, PartialEq)]
pub struct Client {
    name: String,
    balance: i64,
    stocks_count: HashMap<StockType, i32>,
}


#[derive(Debug, Clone, PartialEq)]
pub struct ClientParseError;

impl fmt::Display for ClientParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Client parsing error")
    }
}
impl error::Error for ClientParseError {}


impl From<std::num::ParseIntError> for ClientParseError {
    fn from(_error: std::num::ParseIntError) -> Self {
        ClientParseError
    }
}

impl FromStr for Client {
    type Err = ClientParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let attrs: Vec<&str> = s.split('\t')
                                 .filter(|s| !s.is_empty())
                                 .collect();
    
        if attrs.len() != 6 {
            return Err(ClientParseError);
        } else {
            let name = attrs[0].to_string();
            let balance = attrs[1].parse::<i64>()?;
            let mut stocks_count: HashMap<StockType, i32> = HashMap::new();
            
            stocks_count.insert(StockType::A, attrs[2].parse::<i32>()?);
            stocks_count.insert(StockType::B, attrs[3].parse::<i32>()?);
            stocks_count.insert(StockType::C, attrs[4].parse::<i32>()?);
            stocks_count.insert(StockType::D, attrs[5].parse::<i32>()?);
            
            return Ok(Client {
                name,
                balance,
                stocks_count,
            })
        }
    }
}

impl fmt::Display for Client {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {} {} {} {} {}", 
            self.name,
            self.balance,
            self.stocks_count.get(&StockType::A).unwrap(),
            self.stocks_count.get(&StockType::B).unwrap(),
            self.stocks_count.get(&StockType::C).unwrap(),
            self.stocks_count.get(&StockType::D).unwrap(),
        )
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum OrderApplyError {
    LittleMoney,
    FewStocks,
}

impl fmt::Display for OrderApplyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if *self == OrderApplyError::FewStocks {
            return write!(f, "Client has little money for buying");
        } else {
            return write!(f, "Client has few stocks for saling");
        }
    }
}
impl error::Error for OrderApplyError {}


impl Client {
    pub fn apply(&mut self, is_buying: bool, stock_type: StockType,
                    price: i32, stock_count: i32) -> Result<(), OrderApplyError> {
        let cost = (price as i64) * (stock_count as i64);
        let client_stock_count = self.stocks_count.get_mut(&stock_type).unwrap();

        if is_buying {
            if self.balance < cost {
                return Err(OrderApplyError::LittleMoney);
            }
            self.balance -= cost;
            *client_stock_count += stock_count;
        } else {
            if *client_stock_count < stock_count {
                return Err(OrderApplyError::FewStocks);
            }
            self.balance += cost;
            *client_stock_count -= stock_count;
        }
        Ok(())
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }
}


#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::*;

    #[test]
    fn test_client_from_str() {
        assert_eq!(Client::from_str("C0\t0\t0\t0\t0\t0"), 
            Ok(Client{
                name: "C0".to_string(),
                balance: 0,
                stocks_count: HashMap::from([
                    (StockType::A, 0),
                    (StockType::B, 0),
                    (StockType::C, 0),
                    (StockType::D, 0),
                ],)
            }));
    }

    #[test]
    fn test_client_apply_order() {
        let mut client = Client{
            name: "C0".to_string(),
            balance: 1000,
            stocks_count: HashMap::from([
                (StockType::A, 0),
                (StockType::B, 0),
                (StockType::C, 0),
                (StockType::D, 0),
            ],)
        };
        client.apply(true, StockType::A, 1000, 1).unwrap();
        assert!(client == Client{
            name: "C0".to_string(),
            balance: 0,
            stocks_count: HashMap::from([
                (StockType::A, 1),
                (StockType::B, 0),
                (StockType::C, 0),
                (StockType::D, 0),
            ],)
        })
    }
}