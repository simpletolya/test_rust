mod client;
mod order;
mod stock_type;
mod client_dict;

use std::io::BufWriter;
use std::io::prelude::*;
use std::fs::File;

use client_dict::{parse_clients, apply_orders};

fn main() {

    let mut clients_file = File::open("/home/anatoliy/projects/test_rust/Clients.txt").unwrap();
    let mut clients = parse_clients(&mut clients_file).unwrap();

    let mut orders_file = File::open("/home/anatoliy/projects/test_rust/Orders.txt").unwrap();
    apply_orders(&mut clients, &mut orders_file).unwrap();
    
    let f = File::create("/home/anatoliy/projects/test_rust/result.txt").expect("Unable to create file");
    let mut f = BufWriter::new(f);
    for client in clients.values() {
        f.write_all(client.to_string().as_bytes()).expect("Unable to write data");
    }
}
