use std::{str::FromStr, fmt, error};

use crate::stock_type::{StockType, ParseStockTypeError};

#[derive(Debug)]
#[derive(PartialEq)]
pub struct Order {
    pub client_name: String,
    pub is_buying: bool,
    pub stock_type: StockType,
    pub price: i32,
    pub count: i32,
}

#[derive(Debug, Clone)]
#[derive(PartialEq)]
pub struct OrderParseError;

impl fmt::Display for OrderParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Order parsing error")
    }
}

impl error::Error for OrderParseError {}

impl From<ParseStockTypeError> for OrderParseError {
    fn from(_error: ParseStockTypeError) -> Self {
        OrderParseError
    }
}

impl From<std::num::ParseIntError> for OrderParseError {
    fn from(_error: std::num::ParseIntError) -> Self {
        OrderParseError
    }
}

impl FromStr for Order {
    type Err = OrderParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let attrs: Vec<&str> = s.split('\t')
                                 .filter(|s| !s.is_empty())
                                 .collect();
    
        if attrs.len() != 5 {
            return Err(OrderParseError);
        } else {
            let client_name = attrs[0].to_string();
            let is_buying: bool;
            if attrs[1] == "s" {
                is_buying = false;
            } else if attrs[1] == "b" {
                is_buying = true;
            } else {
                return Err(OrderParseError);
            }
            let stock_type = StockType::from_str(attrs[2])?;
            let price = attrs[3].parse::<i32>()?;
            let count = attrs[4].parse::<i32>()?;
            
            return Ok(Order{
                client_name,
                is_buying,
                stock_type,
                price,
                count,
            })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_order_from_str() {
        assert_eq!(Order::from_str("C0\ts\tA\t100\t100"),
            Ok(Order{
                client_name: "C0".to_string(), 
                is_buying: false, 
                stock_type: StockType::A,
                price: 100,
                count: 100,
            })
        );
    }
}