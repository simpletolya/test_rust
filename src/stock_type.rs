use std::str::FromStr;

#[derive(Debug)]
#[derive(Hash)]
#[derive(PartialEq, Eq)]
pub enum StockType {
    A,
    B,
    C,
    D,
}

pub struct ParseStockTypeError;

impl FromStr for StockType {
    type Err = ParseStockTypeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "A" {
            return Ok(StockType::A);
        } else if s == "B" {
            return Ok(StockType::B);
        } else if s == "C" {
            return Ok(StockType::C);
        } else if s == "D" {
            return Ok(StockType::D);
        }
        return Err(ParseStockTypeError);
    }
}
